package com.console.test;

import com.console.test.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application{

	@Autowired
	TeamRepository teamRepository;

	public static void main(String[] args) {

		SpringApplication app = new SpringApplication(Application.class);

		app.run(args);
	}


}