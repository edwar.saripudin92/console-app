package com.console.test.shell;

import com.console.test.model.Team;
import com.console.test.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.List;

@ShellComponent
public class Main {

    @Autowired
    TeamRepository teamRepository;

    @ShellMethod(value = "Add numbers.")
    public List<Team> add(String name) {
        Team team=new Team();
        team.setName(name);
        teamRepository.save(team);

        return teamRepository.findAll();
    }
}
